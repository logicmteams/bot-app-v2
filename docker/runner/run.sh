#ACTIVE_PROFILE=dev
#ACTIVE_PROFILE=test
#ACTIVE_PROFILE=prod
#ACTIVE_PROFILE=default

PROPERTIES_FILENAMES=app,db

java -version
pwd
cd "$1/$2"
JAR_FILE=$(ls "target/"*".jar")

#java -Dlogback.configurationFile=config/logback.xml -Dlogging.config=config/logback.xml -jar $1.jar --spring.config.location=file:config/ --spring.config.name=default > ./logs/runtime.log.out &
java -jar $JAR_FILE \
--spring.config.location=file:"$1/$2/src/main/resources/profile/" \
--spring.config.name=$PROPERTIES_FILENAMES \
--spring.config.active=$ACTIVE_PROFILE
#java -jar $JAR_FILE --spring.config.location=optional:"$1/$2/src/main/resources/profile/" --spring.config.name=$ACTIVE_PROFILE
