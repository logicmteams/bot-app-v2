mvn -v
java -version
cd "$1"
#remove old .jar
mvn clean
#build new .jar
mvn install
#remove root permission from files
chown -R 1000:1000 .