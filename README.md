1. Use docker to build whole project' services:
```shell
cd docker/builder
docker-compose up #(or docker-compose up --build if need to reload new config from .env after first build)
```
Expected: maven should build whole project with all modules (services) will be package to
jar binary placed in module_path/target/

Remember to run docker-compose up **--build** if make change .env/Dockerfile/entry.sh file

To run custom launching script, edit **docker/runner/run.sh**,
this file is dynamic so no need to add **--build** to docker command

2. How to use docker to run a service:
This step need project was built before do it (mean .jar file was generated in module_path/target/ as step 1)
```shell
cd docker/runner
docker-compose up #(or docker-compose up --build if need to reload new config from .env after first build)
```
Expected: shift-management service will be run, it can be access at **http://localhost:8000**

To change the mapped port **8000** can be change on docker/runner/.env via **LOCAL_PORT_MAP_TO** variables
Remember to run docker-compose up **--build** if make change .env/Dockerfile/entry.sh file

To change profile or run custom launching script, edit **docker/runner/run.sh**
this file is dynamic so no need to add **--build** to docker command
