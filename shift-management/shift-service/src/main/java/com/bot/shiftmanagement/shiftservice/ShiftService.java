package com.hamatim.shiftmanagement.shiftservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Value;

@SpringBootApplication
@RestController
public class ShiftService {

	@Value(value="${config.app.profile}")
	private String profileAppProfile;

	@Value(value="${config.db.profile}")
	private String profileDBProfile;

	public static void main(String[] args) {
		SpringApplication.run(ShiftService.class, args);
	}

	@GetMapping("/")
	public String printServiceName() {
		String template = "App profile: %s<br/>DB profile: %s<br/>From: %s";
		return String.format(template, profileAppProfile, profileDBProfile, getClass());
	}

}